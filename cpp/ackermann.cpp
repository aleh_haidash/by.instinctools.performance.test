// -*- mode: c++ -*-
// $Id: ackermann.g++,v 1.4 2001/07/15 03:37:00 doug Exp $
// http://www.bagley.org/~doug/shootout/

#include <iostream>
#include <stdlib.h>
#include <chrono>

using namespace std;

int Ack(int M, int N) { return (M ? (Ack(M - 1, N ? Ack(M, (N - 1)) : 1)) : N + 1); }

int main(int argc, char *argv[]) {

    int n = ((argc == 2) ? atoi(argv[1]) : 1);

    auto start = chrono::high_resolution_clock::now();

    Ack(3, n);

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::nanoseconds>(finish - start).count() << "ns\n";

    return (0);
}
