// -*- mode: c++ -*-
// $Id: strcat.g++,v 1.4 2001/07/11 02:06:39 doug Exp $
// http://www.bagley.org/~doug/shootout/
// with help from PeterB

#include <iostream>
#include <string>
#include <chrono>

using namespace std;

int main(int argc, char *argv[]) {


    int i, n = ((argc == 2) ? atoi(argv[1]) : 1);
    auto start = chrono::high_resolution_clock::now();
    string str;
//    size_t capacity = 31;
    str.reserve(n * 6); // as per C-string
//    size_t newLength = 6;
    for (i = 0; i < n; i++) {
//        if (newLength > capacity) {
//            capacity *= 2;
//            str.reserve(capacity);
//        }
        str += "hello\n";
//        newLength += 6;
    }
//    cout << str.length() << endl;

    cout << str.length() << endl;
    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::milliseconds>(finish - start).count() << "ms\n";


    return 0;
}
