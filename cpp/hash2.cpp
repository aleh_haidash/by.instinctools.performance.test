// -*- mode: c++ -*-
// $Id: hash2.g++,v 1.2 2001/06/20 03:20:02 doug Exp $
// http://www.bagley.org/~doug/shootout/

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <unordered_map>
#include <chrono>

using namespace std;

struct eqstr {
    bool operator()(const char *s1, const char *s2) const {
        return strcmp(s1, s2) == 0;
    }
};

int main(int argc, char *argv[]) {

    int n = ((argc == 2) ? atoi(argv[1]) : 1);

    auto start = chrono::high_resolution_clock::now();

    char buf[16];
    typedef unordered_map<const char *, int, hash<const char *>, eqstr> HM;
    HM hash1, hash2;

    for (int i = 0; i < 10000; i++) {
        sprintf(buf, "foo_%d", i);
        hash1[strdup(buf)] = i;
    }
    for (int i = 0; i < n; i++) {
        for (HM::iterator k = hash1.begin(); k != hash1.end(); ++k) {
            hash2[(*k).first] += hash1[(*k).first];
        }
    }

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::milliseconds>(finish - start).count() << "ms\n";

    return 0;
}
