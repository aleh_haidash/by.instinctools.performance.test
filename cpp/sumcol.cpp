// -*- mode: c++ -*-
// $Id: sumcol.g++,v 1.6 2001/07/26 00:29:39 doug Exp $
// http://www.bagley.org/~doug/shootout/
// with help from Waldek Hebisch

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <chrono>

using namespace std;

#define MAXLINELEN 128

int main(int argc, char **argv) {

    auto start = chrono::high_resolution_clock::now();

    char line[MAXLINELEN];
    int sum = 0;
    char buff[4096];

    ifstream file;
    file.open("file.txt");

    file.rdbuf()->pubsetbuf(buff, 4096); // enable buffering

    while (file.getline(line, MAXLINELEN)) {
        sum += atoi(line);
    }

    file.close();

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::nanoseconds>(finish - start).count() << "ns\n";

    return 0;
}
