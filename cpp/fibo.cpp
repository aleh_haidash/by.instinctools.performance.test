// -*- mode: c++ -*-
// $Id: fibo.g++,v 1.3 2001/06/20 03:20:02 doug Exp $
// http://www.bagley.org/~doug/shootout/

#include <iostream>
#include <stdlib.h>
#include <chrono>

using namespace std;

unsigned long fib(unsigned long n) {
    if (n < 2)
        return (1);
    else
        return (fib(n - 2) + fib(n - 1));
}

int main(int argc, char *argv[]) {

    int n = ((argc == 2) ? atoi(argv[1]) : 1);

    auto start = chrono::high_resolution_clock::now();

    fib(n);

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::nanoseconds>(finish - start).count() << "ns\n";

    return (0);
}
