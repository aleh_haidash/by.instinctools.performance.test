// -*- mode: c++ -*-
// $Id: random.g++,v 1.7 2001/07/31 19:47:03 doug Exp $
// http://www.bagley.org/~doug/shootout/

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <chrono>

using namespace std;

#define IM 139968
#define IA 3877
#define IC 29573

inline double gen_random(double max) {
    static long last = 42;
    last = (last * IA + IC) % IM;
    return (max * last / IM);
}

int main(int argc, char *argv[]) {


    int N = ((argc == 2) ? atoi(argv[1]) : 1);
    auto start = chrono::high_resolution_clock::now();
    double result = 0;

    while (N--) {
        result = gen_random(100.0);
    }
    cout.precision(9);
    cout.setf(ios::fixed);
//    cout << result << endl;

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::milliseconds>(finish - start).count() << "ms\n";

    return 0;
}

