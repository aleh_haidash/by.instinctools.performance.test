// -*- mode: c++ -*-
// $Id: nestedloop.g++,v 1.2 2001/06/20 03:20:02 doug Exp $
// http://www.bagley.org/~doug/shootout/

#include <iostream>
#include <stdlib.h>
#include <chrono>

using namespace std;

int main(int argc, char *argv[]) {


    int n = ((argc == 2) ? atoi(argv[1]) : 1);
    auto start = chrono::high_resolution_clock::now();
    int a, b, c, d, e, f, x = 0;

    for (a = 0; a < n; a++)
        for (b = 0; b < n; b++)
            for (c = 0; c < n; c++)
                for (d = 0; d < n; d++)
                    for (e = 0; e < n; e++)
                        for (f = 0; f < n; f++)
                            x++;

    auto finish = chrono::high_resolution_clock::now();

    cout << chrono::duration_cast<chrono::milliseconds>(finish - start).count() << "ms\n";

    cout << x;

    return (0);
}
