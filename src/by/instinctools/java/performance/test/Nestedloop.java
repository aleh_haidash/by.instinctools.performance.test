package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Nestedloop {
    private static final int N = 50;

    @Benchmark
    public int measureName() {
        int x = 0;
        for (int a = 0; a < N; a++)
            for (int b = 0; b < N; b++)
                for (int c = 0; c < N; c++)
                    for (int d = 0; d < N; d++)
                        for (int e = 0; e < N; e++)
                            for (int f = 0; f < N; f++)
                                x++;

        return x;
    }
}
