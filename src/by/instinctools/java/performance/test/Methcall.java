package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Methcall {

    private static final int N = 1_000_000_000;

    @Benchmark
    public boolean measureName() {
        boolean res;
        boolean val = true;
        Toggle toggle = new Toggle(val);
        for (int i = 0; i < N; i++) {
            val = toggle.activate().value();
        }

        res = val;

        val = true;
        NthToggle ntoggle = new NthToggle(true, 3);
        for (int i = 0; i < N; i++) {
            val = ntoggle.activate().value();
        }

        return res && val;
    }

    static class Toggle {
        boolean state = true;

        public Toggle(boolean start_state) {
            this.state = start_state;
        }

        public boolean value() {
            return (this.state);
        }

        public Toggle activate() {
            this.state = !this.state;
            return (this);
        }
    }

   final static class NthToggle extends Toggle {
        int count_max = 0;
        int counter = 0;

        public NthToggle(boolean start_state, int max_counter) {
            super(start_state);
            this.count_max = max_counter;
            this.counter = 0;
        }

        public Toggle activate() {
            this.counter += 1;
            if (this.counter >= this.count_max) {
                this.state = !this.state;
                this.counter = 0;
            }
            return (this);
        }
    }
}