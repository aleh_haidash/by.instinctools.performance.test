package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Sumcol {

    @Benchmark
    public int measureName() {
        int sum = 0;

        try {
            List<String> lines = Files.readAllLines(Paths.get(Sumcol.class.getResource("/file.txt").getPath()));
            for (String line : lines) {
                sum = sum + Integer.parseInt(line);
            }
        } catch (IOException e) {
            System.err.println(e);
            return -1;
        }
        return sum;
    }
}
