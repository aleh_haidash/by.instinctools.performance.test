package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.lang.reflect.Array;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Matrix {
    private static final int SIZE = 30;
    private static final int N = 100;

    @Benchmark
    public static void test() {
        int m1[][] = mkmatrix(SIZE, SIZE);
        int m2[][] = mkmatrix(SIZE, SIZE);
        int mm[][] = new int[SIZE][SIZE];
        for (int i = 0; i < N; i++) {
            mmult(m1, m2, mm);
        }
    }

    public static int[][] mkmatrix(int rows, int cols) {
        int count = 1;
        int m[][] = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                m[i][j] = count++;
            }
        }
        return (m);
    }

    public static void mmult(int[][] m1, int[][] m2, int[][] m3) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                int val = 0;
                for (int k = 0; k < SIZE; k++) {
                    val += m1[i][k] * m2[k][j];
                }
                m3[i][j] = val;
            }
        }
    }
}
