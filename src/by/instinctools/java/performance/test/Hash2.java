package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Hash2 {

    private static final int N = 10;

    @Benchmark
    public void test() {

        HashMap<String, Val> hash1 = new HashMap<>(10000);
        HashMap<String, Val> hash2 = new HashMap<>(N);

        for (int i = 0; i < 10000; i++) {
            hash1.put("foo_" + Integer.toString(i, 10), new Val(i));
        }

        for (int i = 0; i < N; i++) {
            for (HashMap.Entry<String, Val> entry : hash1.entrySet()) {
                String key = entry.getKey();
                int v1 = entry.getValue().val;
                if (hash2.containsKey(key)) {
                    hash2.get(key).val += v1;
                } else {
                    hash2.put(key, new Val(v1));
                }
            }
        }
    }

    static class Val {
        int val;
        Val(int init) {
            val = init;
        }
    }
}
