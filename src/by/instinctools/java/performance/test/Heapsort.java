package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Heapsort {

    public static final long IM = 139968;
    public static final long IA = 3877;
    public static final long IC = 29573;

    public static final int N = 100000;

    public static long last = 42;

    @Benchmark
    public void test() {
        double[] ary = new double[ N + 1];
        for (int i = 1; i <= N; i++) {
            ary[i] = gen_random(1);
        }
        heapsort(N, ary);
    }

    public static double gen_random(double max) {
        return (max * (last = (last * IA + IC) % IM) / IM);
    }

    public static void heapsort(int n, double ra[]) {
        int l, j, ir, i;
        double rra;

        l = (n >> 1) + 1;
        ir = n;
        for (; ; ) {
            if (l > 1) {
                rra = ra[--l];
            } else {
                rra = ra[ir];
                ra[ir] = ra[1];
                if (--ir == 1) {
                    ra[1] = rra;
                    return;
                }
            }
            i = l;
            j = l << 1;
            while (j <= ir) {
                if (j < ir && ra[j] < ra[j + 1]) {
                    ++j;
                }
                if (rra < ra[j]) {
                    ra[i] = ra[j];
                    j += (i = j);
                } else {
                    j = ir + 1;
                }
            }
            ra[i] = rra;
        }
    }
}
