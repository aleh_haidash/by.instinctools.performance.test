package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Sieve {
    private static int N = 1_000_000;

    @Benchmark
    public int measureName() {
        boolean[] flags = new boolean[8192 + 1];
        int count = 0;
        while (N-- > 0) {
            count = 0;
            for (int i = 2; i <= 8192; i++) {
                flags[i] = true;
            }
            for (int i = 2; i <= 8192; i++) {
                if (flags[i]) {
                    // remove all multiples of prime: i
                    for (int k = i + i; k <= 8192; k += i) {
                        flags[k] = false;
                    }
                    count++;
                }
            }
        }
        return count;
    }
}
