package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Hash {

    private static final int N = 100000;

    @Benchmark
    public int test() {

        int i;
        int c= 0;

        HashMap<String, Integer> ht = new HashMap<>();

        for (i = 1; i <= N; i++) {
            ht.put(Integer.toString(i, 16), i);
        }

        for (i = 1; i <= N; i++) {
            if (ht.containsKey(Integer.toString(i, 10))) {
                c++;
            }
        }
        return c;
    }
}
