package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Objinst {

    private static final int N = 2_000_000_000;

    @Benchmark
    public int measureName() {

        int res = 0;

        Toggle toggle1 = new Toggle(true);
        for (int i = 0; i < 5; i++) {
            toggle1.activate().value();
        }
        res += toggle1.value() ? 1 : 0;
        for (int i = 0; i < N; i++) {
            Toggle toggle = new Toggle(true);
            res += toggle.value() ? 1 : 0;
        }

        NthToggle ntoggle1 = new NthToggle(true, 3);
        for (int i = 0; i < 8; i++) {
            ntoggle1.activate().value();
        }
        res += toggle1.value() ? 1 : 0;
        for (int i = 0; i < N; i++) {
            NthToggle toggle = new NthToggle(true, 3);
            res += toggle.value() ? 1 : 0;
        }
        return res;
    }

    static class Toggle {
        boolean state = true;

        public Toggle(boolean start_state) {
            this.state = start_state;
        }

        public boolean value() {
            return (this.state);
        }

        public Toggle activate() {
            this.state = !this.state;
            return (this);
        }
    }

    final static class NthToggle extends Toggle {
        int count_max = 0;
        int counter = 0;

        public NthToggle(boolean start_state, int max_counter) {
            super(start_state);
            this.count_max = max_counter;
            this.counter = 0;
        }

        public Toggle activate() {
            this.counter += 1;
            if (this.counter >= this.count_max) {
                this.state = !this.state;
                this.counter = 0;
            }
            return (this);
        }
    }
}
