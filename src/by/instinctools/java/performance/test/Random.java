package by.instinctools.java.performance.test;

import org.openjdk.jmh.annotations.*;

import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10)
@Fork(value = 1)
@Measurement(iterations = 5)
public class Random {

    public static final long IM = 139968;
    public static final long IA = 3877;
    public static final long IC = 29573;
    private static int N = 1_000_000_000;

    @Benchmark
    public double measureName() {
        double i = 0.0;
        long timeMillis = System.currentTimeMillis();

        while (N-- > 0) {
            i += gen_random(timeMillis);
        }
        return i;
    }

    public static long last = 42;

    public static double gen_random(double max) {
        return (max * (last = (last * IA + IC) % IM) / IM);
    }
}